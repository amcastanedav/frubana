import math


def format_median(number):
    parts = math.modf(number)
    if abs(parts[0]) > 0:
        return str(number)
    else:
        return str(int(number))


def find_median(numbers):
    numbers.sort()
    size = len(numbers)
    if (size % 2) == 0:
        n1 = numbers[int(size / 2 - 1)]
        n2 = numbers[int(size / 2)]
        return (n1 + n2) / 2
    else:
        i = int((size + 1) / 2 - 1)
        return numbers[i]


def remove(numbers, number):
    if number in numbers:
        numbers.remove(number)
        if len(numbers) > 0:
            print(format_median(find_median(numbers)))
        else:
            print("Wrong!")
    else:
        print("Wrong!")


def main():
    numbers = []
    n = int(input())
    for i in range(n):
        line = input().split(" ")
        number = int(line[1])
        if line[0] == 'a':
            numbers.append(number)
            print(format_median(find_median(numbers)))
        elif line[0] == 'r':
            remove(numbers, number)
        else:
            print("Wrong!")


if __name__ == "__main__":
    main()
