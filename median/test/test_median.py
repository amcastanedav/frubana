import unittest

from median import find_median


class TestMedian(unittest.TestCase):
    def test_find_median_uneven_set(self):
        data = [3, 0, 1]
        result = find_median(data)
        self.assertEqual(1, result)

    def test_find_median_even_set(self):
        data = [3, 1]
        result = find_median(data)
        self.assertEqual(2.0, result)

    def test_find_median_uneven_set_with_negative_numbers(self):
        data = [-5, -10, 0]
        result = find_median(data)
        self.assertEqual(-5, result)

    def test_find_median_even_set_with_decimals(self):
        data = [5, 6, 9, 3]
        result = find_median(data)
        self.assertEqual(5.5, result)

    def test_find_median_even_set_with_negative_numbers(self):
        data = [-5, -12, 0, 4]
        result = find_median(data)
        self.assertEqual(-2.5, result)


if __name__ == '__main__':
    unittest.main()
