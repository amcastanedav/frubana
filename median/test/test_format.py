import unittest

from median import format_median


class TestFormat(unittest.TestCase):
    def test_format_median_int(self):
        result = format_median(5)
        self.assertEqual("5", result)

    def test_format_median_int_negative(self):
        result = format_median(-7)
        self.assertEqual("-7", result)

    def test_format_median_int_with_decimal_zero_part(self):
        result = format_median(3.0)
        self.assertEqual("3", result)

    def test_format_median_int_with_decimal_zero_part_negative(self):
        result = format_median(-6.0)
        self.assertEqual("-6", result)

    def test_format_median_with_decimal_non_zero_part(self):
        result = format_median(5.6000)
        self.assertEqual("5.6", result)

    def test_format_median_with_decimal_non_zero_part_negative(self):
        result = format_median(-6.25000)
        self.assertEqual("-6.25", result)


if __name__ == '__main__':
    unittest.main()
