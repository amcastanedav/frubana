class Node:
    def __init__(self, index, color):
        self.index = index
        self.color = color
        self.paths = []

    def add_path(self, node):
        self.paths.append(node)


def create_nodes(n, line):
    nodes = []
    colors = line.split(" ")
    for i in range(n):
        nodes.append(Node(i, colors[i]))
    return nodes


def create_path(nodes, line):
    path = line.split(" ")
    for node in nodes:
        if node.index == (int(path[0]) - 1):
            node.add_path(int(path[1]) - 1)


def enlarge_path(path, next_node):
    new_path = list(path)
    new_path.append(next_node)
    return new_path


def add_new_paths(path, paths, node):
    for next_node in node.paths:
        if path[-1] == node.index and not next_node in path:
            paths.append(enlarge_path(path, next_node))
        if path[-1] == next_node and not node.index in path:
            paths.append(enlarge_path(path, node.index))


def explore_paths(paths, nodes):
    for node in nodes:
        for path in paths:
            add_new_paths(path, paths, node)


def init_paths(nodes):
    paths = []
    for node in nodes:
        for path in node.paths:
            paths.append([node.index, path])
            paths.append([path, node.index])
    return paths


def find_path(start_index, end_index, paths):
    for path in paths:
        if path[0] == start_index and path[-1] == end_index:
            return path
        if path[-1] == start_index and path[0] == end_index:
            return path
    else:
        return []


def count_colors(path, nodes):
    colors = set()
    for node in nodes:
        if node.index in path:
            colors.add(node.color)
    return len(colors)


def calculate_sum(current_node, nodes, paths):
    node_sum = 0
    for node in nodes:
        if node.index == current_node.index:
            node_sum += 1
        else:
            path = find_path(current_node.index, node.index, paths)
            node_sum += count_colors(path, nodes)
    return node_sum


def main():
    n = int(input())
    nodes = create_nodes(n, input())
    for i in range(n - 1):
        create_path(nodes, input())
    paths = init_paths(nodes)
    explore_paths(paths, nodes)
    for node in nodes:
        print(str(calculate_sum(node, nodes, paths)))


if __name__ == "__main__":
    main()
