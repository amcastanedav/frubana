import unittest

from sum import Node
from sum import init_paths
from sum import explore_paths
from sum import find_path


class TestFindPath(unittest.TestCase):
    def test_find_path_plane_chain(self):
        nodes = [Node(0, 7), Node(1, 4), Node(2, 7), Node(3, 4)]
        nodes[0].add_path(1)
        nodes[1].add_path(2)
        nodes[2].add_path(3)
        paths = init_paths(nodes)
        explore_paths(paths, nodes)

        self.assertTrue([0, 1] == find_path(0, 1, paths) or [1, 0] == find_path(0, 1, paths))
        self.assertTrue([0, 1, 2] == find_path(0, 2, paths) or [2, 1, 0] == find_path(0, 2, paths))
        self.assertTrue([0, 1, 2, 3] == find_path(0, 3, paths) or [3, 2, 1, 0] == find_path(0, 3, paths))

        self.assertTrue([0, 1, 2, 3] == find_path(3, 0, paths) or [3, 2, 1, 0] == find_path(3, 0, paths))
        self.assertTrue([1, 2, 3] == find_path(3, 1, paths) or [3, 2, 1] == find_path(3, 1, paths))
        self.assertTrue([2, 3] == find_path(3, 2, paths) or [3, 2] == find_path(3, 2, paths))

    def test_explore_paths_root_tree(self):
        nodes = [Node(0, 6), Node(1, -5), Node(2, -5), Node(3, 0), Node(4, 6)]
        nodes[0].add_path(1)
        nodes[0].add_path(3)
        nodes[1].add_path(2)
        nodes[3].add_path(4)
        paths = init_paths(nodes)
        explore_paths(paths, nodes)

        self.assertTrue([0, 1] == find_path(0, 1, paths) or [1, 0] == find_path(0, 1, paths))
        self.assertTrue([0, 1, 2] == find_path(0, 2, paths) or [2, 1, 0] == find_path(0, 2, paths))
        self.assertTrue([0, 3] == find_path(0, 3, paths) or [3, 0] == find_path(0, 3, paths))
        self.assertTrue([0, 3, 4] == find_path(0, 4, paths) or [4, 3, 0] == find_path(0, 4, paths))

        self.assertTrue([1, 0] == find_path(1, 0, paths) or [0, 1] == find_path(1, 0, paths))
        self.assertTrue([1, 2] == find_path(1, 2, paths) or [2, 1] == find_path(1, 2, paths))
        self.assertTrue([1, 0, 3] == find_path(1, 3, paths) or [3, 0, 1] == find_path(1, 3, paths))
        self.assertTrue([1, 0, 3, 4] == find_path(1, 4, paths) or [4, 3, 0, 1] == find_path(1, 4, paths))

        self.assertTrue([4, 3, 0] == find_path(4, 0, paths) or [0, 3, 4] == find_path(4, 0, paths))
        self.assertTrue([4, 3, 0, 1] == find_path(4, 1, paths) or [1, 0, 3, 4] == find_path(4, 1, paths))
        self.assertTrue([4, 3, 0, 1, 2] == find_path(4, 2, paths) or [2, 1, 0, 3, 4] == find_path(4, 2, paths))
        self.assertTrue([4, 3] == find_path(4, 3, paths) or [3, 4] == find_path(4, 3, paths))


if __name__ == '__main__':
    unittest.main()
