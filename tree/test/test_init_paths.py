import unittest

from sum import Node
from sum import init_paths


class TestInitPaths(unittest.TestCase):
    def test_init_paths_plane_chain(self):
        nodes = [Node(0, 7), Node(1, 4), Node(2, 7), Node(3, 4)]
        nodes[0].add_path(1)
        nodes[1].add_path(2)
        nodes[2].add_path(3)
        paths = init_paths(nodes)
        self.assertEqual(6, len(paths))
        self.assertTrue([0, 1] in paths)
        self.assertTrue([1, 0] in paths)
        self.assertTrue([1, 2] in paths)
        self.assertTrue([2, 1] in paths)
        self.assertTrue([2, 3] in paths)
        self.assertTrue([3, 2] in paths)

    def test_init_paths_root_tree(self):
        nodes = [Node(0, 6), Node(1, -5), Node(2, -5), Node(3, 0), Node(4, 6)]
        nodes[0].add_path(1)
        nodes[0].add_path(3)
        nodes[1].add_path(2)
        nodes[3].add_path(4)
        paths = init_paths(nodes)
        self.assertEqual(8, len(paths))
        self.assertTrue([0, 1] in paths)
        self.assertTrue([1, 0] in paths)
        self.assertTrue([0, 3] in paths)
        self.assertTrue([3, 0] in paths)
        self.assertTrue([1, 2] in paths)
        self.assertTrue([2, 1] in paths)
        self.assertTrue([3, 4] in paths)
        self.assertTrue([4, 3] in paths)


if __name__ == '__main__':
    unittest.main()
