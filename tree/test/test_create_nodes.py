import unittest

from sum import create_nodes


class TestCreateNodes(unittest.TestCase):
    def test_create_nodes(self):
        result = create_nodes(6, "-8 9 5 -2 1 9")
        self.assertEqual(6, len(result))
        self.assertEqual('-8', result[0].color)
        self.assertEqual('9', result[1].color)
        self.assertEqual('5', result[2].color)
        self.assertEqual('-2', result[3].color)
        self.assertEqual('1', result[4].color)
        self.assertEqual('9', result[5].color)


if __name__ == '__main__':
    unittest.main()