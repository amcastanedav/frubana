import unittest

from sum import Node
from sum import count_colors


class TestCountColors(unittest.TestCase):
    def test_count_colors_plane_chain(self):
        nodes = [Node(0, 7), Node(1, 4), Node(2, 7), Node(3, 4)]
        nodes[0].add_path(1)
        nodes[1].add_path(2)
        nodes[2].add_path(3)

        self.assertEqual(2, count_colors([0, 1], nodes))
        self.assertEqual(2, count_colors([0, 1, 2], nodes))
        self.assertEqual(2, count_colors([0, 1, 2, 3], nodes))

        self.assertEqual(2, count_colors([1, 0], nodes))
        self.assertEqual(2, count_colors([1, 2], nodes))
        self.assertEqual(2, count_colors([1, 2, 3], nodes))

    def test_explore_paths_root_tree(self):
        nodes = [Node(0, 6), Node(1, -5), Node(2, -5), Node(3, 0), Node(4, 6)]
        nodes[0].add_path(1)
        nodes[0].add_path(3)
        nodes[1].add_path(2)
        nodes[3].add_path(4)

        self.assertEqual(2, count_colors([0, 1], nodes))
        self.assertEqual(2, count_colors([0, 1, 2], nodes))
        self.assertEqual(2, count_colors([0, 3], nodes))
        self.assertEqual(2, count_colors([0, 3, 4], nodes))

        self.assertEqual(2, count_colors([1, 0], nodes))
        self.assertEqual(1, count_colors([1, 2], nodes))
        self.assertEqual(3, count_colors([1, 0, 3], nodes))
        self.assertEqual(3, count_colors([1, 0, 3, 4], nodes))

        self.assertTrue(2, count_colors([4, 3, 0], nodes))
        self.assertTrue(3, count_colors([4, 3, 0, 1], nodes))
        self.assertTrue(3, count_colors([4, 3, 0, 1, 2], nodes))
        self.assertTrue(2, count_colors([4, 3], nodes))


if __name__ == '__main__':
    unittest.main()
