import unittest

from sum import Node
from sum import create_path


class TestCreatePaths(unittest.TestCase):
    def test_create_paths(self):
        nodes = [Node(0, 3), Node(1, 6), Node(2, 9)]
        create_path(nodes, "1 2")
        create_path(nodes, "2 3")
        self.assertEqual(1, len(nodes[0].paths))
        self.assertEqual(1, nodes[0].paths[0])
        self.assertEqual(1, len(nodes[1].paths))
        self.assertEqual(2, nodes[1].paths[0])
        self.assertEqual(0, len(nodes[2].paths))


if __name__ == '__main__':
    unittest.main()
