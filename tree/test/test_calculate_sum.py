import unittest

from sum import Node
from sum import init_paths
from sum import explore_paths
from sum import calculate_sum


class TestCalculateSum(unittest.TestCase):
    def test_calculate_sum_plane_chain(self):
        nodes = [Node(0, 7), Node(1, 4), Node(2, 7), Node(3, 4)]
        nodes[0].add_path(1)
        nodes[1].add_path(2)
        nodes[2].add_path(3)
        paths = init_paths(nodes)
        explore_paths(paths, nodes)

        self.assertEqual(7, calculate_sum(nodes[0], nodes, paths))
        self.assertEqual(7, calculate_sum(nodes[1], nodes, paths))
        self.assertEqual(7, calculate_sum(nodes[2], nodes, paths))
        self.assertEqual(7, calculate_sum(nodes[3], nodes, paths))

    def test_calculate_sum_root_tree(self):
        nodes = [Node(0, 6), Node(1, -5), Node(2, -5), Node(3, 0), Node(4, 6)]
        nodes[0].add_path(1)
        nodes[0].add_path(3)
        nodes[1].add_path(2)
        nodes[3].add_path(4)
        paths = init_paths(nodes)
        explore_paths(paths, nodes)

        self.assertEqual(9, calculate_sum(nodes[0], nodes, paths))
        self.assertEqual(10, calculate_sum(nodes[1], nodes, paths))
        self.assertEqual(10, calculate_sum(nodes[2], nodes, paths))
        self.assertEqual(11, calculate_sum(nodes[3], nodes, paths))
        self.assertEqual(11, calculate_sum(nodes[4], nodes, paths))

if __name__ == '__main__':
    unittest.main()
