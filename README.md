# Frubana

Hi everyone this project contains two modules solving two algorithm problems and their test
This exercise was made as a technical test to Frubana

## Getting Started

To run you just need in the console:

```
python3 median.py
python3 sum.py
```

### Prerequisites

You need python 3.7

